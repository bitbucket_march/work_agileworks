﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DTO
{
    public class SupportTicketBLLDTO
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }
        public string Body { get; set; }

        public DateTime TimeCreated { get; set; }
        public string TimeCreatedString
        {
            get
            {
                return TimeCreated.ToString("dd/MM/yyyy HH:mm");
            }
        }
        [Required]
        [DisplayFormat(DataFormatString = "{0: dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true )]
        public DateTime DueTime { get; set; }
        public string DueTimeString
        {
            get {
                return DueTime.ToString("dd/MM/yyyy HH:mm");
            }
        }

        public bool IsCritical { get; set; }
        public bool IsCompleted { get; set; }

    }
}
