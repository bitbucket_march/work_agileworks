﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class SupportTicketDALDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Body { get; set; }

        public DateTime TimeCreated{ get; set; }
        public DateTime DueTime { get; set; }

        public bool IsVisible { get; set; }
    }
}
