using System.Threading.Tasks;
using BLL.Interfaces;
using DTO;
using Microsoft.AspNetCore.Mvc;

namespace CSupportWebApp.Controllers
{
    public class TicketsController : Controller
    {
        private readonly IBLL _bll;

        public TicketsController(IBLL bll)
        {
            _bll = bll;
        }

        // GET: Tickets
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Tickets.GetAllVisibleTicketsAsync());
        }


        // GET: Tickets/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,Body,DueTime")] SupportTicketBLLDTO supportTicket)
        {
            if (ModelState.IsValid)
            {
                await _bll.Tickets.AddAsync(supportTicket);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(supportTicket);
        }

        public async Task<IActionResult> CloseTicket(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var ticket = await _bll.Tickets.FindAsync(id);
            _bll.Tickets.CloseTicket(ticket);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
