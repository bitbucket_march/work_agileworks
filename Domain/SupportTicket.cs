﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class SupportTicket : BaseEntity
    {
        [Required]
        [MinLength(1)]
        [MaxLength(64)]
        public string Title { get; set; }
        [MinLength(1)]
        [MaxLength(1024)]
        public string Body { get; set; }
        public DateTime TimeCreated { get; set; }
        [Required]
        public DateTime DueTime { get; set; }
        public bool IsVisible { get; set; }
    }
}
