﻿using DAL.Interfaces.Base;
using DAL.Interfaces.Helpers;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAL.Base
{
    public class BaseUow<TDbContext> : IBaseUow
        where TDbContext : DbContext
    {
        protected readonly TDbContext UowDbContext;
        protected readonly IBaseRepositoryProvider _repositoryProvider;

        public BaseUow (TDbContext dataContext, IBaseRepositoryProvider repositoryProvider)
        {
            _repositoryProvider = repositoryProvider;
            UowDbContext = dataContext;
        }

        public IBaseRepository<TDALEntity> BaseRepository<TDALEntity, TDomainEntity>()
            where TDomainEntity : class, IBaseEntity, new()
            where TDALEntity : class, new()
        {
            return _repositoryProvider.GetEntityRepository<TDALEntity, TDomainEntity>();
        }

        public virtual int SaveChanges()
        {
            return UowDbContext.SaveChanges();
        }

        public virtual async Task<int> SaveChangesAsync()
        {
            return await UowDbContext.SaveChangesAsync();
        }
    }
}
