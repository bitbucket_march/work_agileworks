﻿using AutoMapper;
using DAL.Interfaces.Base;

namespace DAL.Base
{
    public class BaseMapper<TDALEntity, TDomainEntity> : IBaseMapper
        where TDALEntity : class, new()
        where TDomainEntity : class, new()

    {
        private readonly IMapper _mapper;
        public BaseMapper()
        {
            _mapper = new MapperConfiguration(config =>
            {
                config.CreateMap<TDALEntity, TDomainEntity>();
                config.CreateMap<TDomainEntity, TDALEntity>();
            }
            ).CreateMapper();
        }
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            return _mapper.Map<TOutObject>(inObject);
        }
    }
}
