﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System;
using DAL.Interfaces.Base;
using Domain.Interfaces;

namespace DAL.Base
{

    // Andres Käver @ ITCollege
    public class BaseRepository<TDALEntity, TDomainEntity, TDbContext> :
        BaseRepository<TDALEntity, TDomainEntity, TDbContext, int>
        where TDALEntity : class, new()
        where TDomainEntity : class, IBaseEntity, new()
        where TDbContext : DbContext
    {
        public BaseRepository(TDbContext repositoryDbContext, IBaseMapper mapper) 
            : base(repositoryDbContext, mapper)
        {
        }
    }

    public class BaseRepository<TDALEntity, TDomainEntity, TDbContext, TKey> : IBaseRepository<TDALEntity>
       where TDALEntity : class, new()
       where TDomainEntity : class, IBaseEntity<TKey>, new()
       where TDbContext : DbContext
       where TKey : IComparable
    {
        protected readonly TDbContext RepositoryDbContext;
        protected readonly DbSet<TDomainEntity> RepositoryDbSet;

        private readonly IBaseMapper _mapper;

        protected readonly IDictionary<TKey, TDomainEntity> EntityCreationCache = new Dictionary<TKey, TDomainEntity>();


        public BaseRepository(TDbContext repositoryDbContext, IBaseMapper mapper)
        {
            RepositoryDbContext = repositoryDbContext;
            _mapper = mapper;

            RepositoryDbSet = RepositoryDbContext.Set<TDomainEntity>();
        }


        #region ASYNC
        public virtual async Task<List<TDALEntity>> AllAsync()
        {
            return (await RepositoryDbSet.ToListAsync())
                .Select(e => _mapper.Map<TDALEntity>(e)).ToList();

        }

        public virtual async Task<TDALEntity> FindAsync(params object[] id)
        {
            return _mapper.Map<TDALEntity>((await RepositoryDbSet.FindAsync(id)));
        }

        public virtual async Task<TDALEntity> AddAsync(TDALEntity entity)
        {
            var res = (await RepositoryDbSet.AddAsync(_mapper.Map<TDomainEntity>(entity))).Entity;
            EntityCreationCache.Add(res.Id, res);
            return _mapper.Map<TDALEntity>(res);
        }
        #endregion
        #region SYNCHRONOUS
        public List<TDALEntity> All()
        {
            return RepositoryDbSet.Select(e => _mapper.Map<TDALEntity>(e)).ToList();
        }

        public TDALEntity Find(params object[] id)
        {
            return _mapper.Map<TDALEntity>(RepositoryDbSet.Find(id));
        }

        public TDALEntity Add(TDALEntity entity)
        {
            return _mapper.Map<TDALEntity>(RepositoryDbSet.Add(_mapper.Map<TDomainEntity>(entity)).Entity);
        }
        #endregion
        #region COMMON
        public virtual TDALEntity Update(TDALEntity entity)
        {
            return _mapper.Map<TDALEntity>(RepositoryDbSet.Update(_mapper.Map<TDomainEntity>(entity)).Entity);
        }

        public virtual void Remove(TDALEntity entity)
        {
            RepositoryDbSet.Remove(_mapper.Map<TDomainEntity>(entity));
        }

        public virtual void Remove(params object[] id)
        {
            var entity = FindAsync(id).Result;
            Remove(entity);
        }
        #endregion
    }
}
