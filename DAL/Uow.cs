﻿using DAL.Base;
using DAL.Interfaces;
using DAL.Interfaces.Helpers;

namespace DAL
{
    public class Uow : BaseUow<AppDbContext>, IUow
    {
        public Uow(AppDbContext dbContext, IBaseRepositoryProvider repositoryProvider)
            : base(dbContext, repositoryProvider)
        { }
        public ISupportTicketRepository Tickets => 
            _repositoryProvider.GetRepository<ISupportTicketRepository>();
    }
} 
