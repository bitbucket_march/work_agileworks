﻿using DAL.Interfaces;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Helpers
{
    public class AppRepositoryFactory : BaseRepositoryFactory<AppDbContext>
    {
        public AppRepositoryFactory()
        {
            AddToCreationMethods<ISupportTicketRepository>(dataContext => new SupportTicketRepository(dataContext));
        }
    }
}
