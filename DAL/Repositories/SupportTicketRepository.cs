﻿using DAL.Base;
using DAL.Interfaces;
using DAL.Mappers;
using DTO;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
     public class SupportTicketRepository : BaseRepository<SupportTicketDALDTO, Domain.SupportTicket, AppDbContext>, 
        ISupportTicketRepository
    {
        public SupportTicketRepository(AppDbContext repositoryDbContext) 
            : base(repositoryDbContext, new SupportTicketMapper()) { }
        public async Task<List<SupportTicketDALDTO>> GetAllVisibleTicketsAsync()
        {
            return await RepositoryDbSet
                .Where(w => w.IsVisible == true)
                .Select(w => SupportTicketMapper.MapFromDomain(w))
                .OrderBy(o => o.DueTime)
                .ToListAsync();
        }
    }   
}
