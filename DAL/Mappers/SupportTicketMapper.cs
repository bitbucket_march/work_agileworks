﻿using DTO;
using System;
using DAL.Interfaces.Base;

namespace DAL.Mappers
{
    public class SupportTicketMapper : IBaseMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(SupportTicketDALDTO))
            {
                return MapFromDomain((Domain.SupportTicket)inObject) as TOutObject;
            }
            if (typeof(TOutObject) == typeof(Domain.SupportTicket))
            {
                return MapFromDAL((SupportTicketDALDTO)inObject) as TOutObject;
            }
            throw new InvalidCastException($"Can't convert from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");

        }

        public static SupportTicketDALDTO MapFromDomain(Domain.SupportTicket supportTicket)
        {
            var res = supportTicket == null ? null : new SupportTicketDALDTO
            {
                Id = supportTicket.Id,
                Title = supportTicket.Title,
                Body = supportTicket.Body,
                TimeCreated = supportTicket.TimeCreated,
                DueTime = supportTicket.DueTime,
                IsVisible = supportTicket.IsVisible
            };
            return res;
        }

        public static Domain.SupportTicket MapFromDAL(SupportTicketDALDTO supportTicket)
        {
            var res = supportTicket == null ? null : new Domain.SupportTicket
            {
                Id = supportTicket.Id,
                Title = supportTicket.Title,
                Body = supportTicket.Body,
                TimeCreated = supportTicket.TimeCreated,
                DueTime = supportTicket.DueTime,
                IsVisible = supportTicket.IsVisible
            };
            return res;
        }
    }
}
