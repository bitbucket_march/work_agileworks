﻿using DAL.Interfaces.Base;

namespace DAL.Interfaces
{
    public interface IUow : IBaseUow
    {
        ISupportTicketRepository Tickets { get; }
    }
}
