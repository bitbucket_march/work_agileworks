﻿using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IDataContext
    {
        Task<int> SaveChangesAsync();
        int SaveChanges();

    }
}
