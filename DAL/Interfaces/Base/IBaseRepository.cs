﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces.Base
{
    // Full CRUD - both async and non-async methods
    public interface IBaseRepository<TDALEntity> : IBaseRepositoryAsync<TDALEntity>,
        IBaseRepositorySynchronous<TDALEntity>
        where TDALEntity : class, new()
    {

    }

    // Async and Common methods
    public interface IBaseRepositoryAsync<TDALEntity> : IBaseRepositoryCommon<TDALEntity>
        where TDALEntity : class, new()
    {
        Task<List<TDALEntity>> AllAsync();
        Task<TDALEntity> FindAsync(params object[] id);
        Task<TDALEntity> AddAsync(TDALEntity entity);
    }

    // Non-async and Common methods
    public interface IBaseRepositorySynchronous<TDALEntity> : IBaseRepositoryCommon<TDALEntity>
        where TDALEntity : class, new()
    {
        List<TDALEntity> All();
        TDALEntity Find(params object[] id);
        TDALEntity Add(TDALEntity entity);
    }

    // Common methods for both synchronous and async repos
    public interface IBaseRepositoryCommon<TDALEntity>
    where TDALEntity : class, new()
    {
        TDALEntity Update(TDALEntity entity);
        void Remove(TDALEntity entity);
        void Remove(params object[] id);
    }

}
