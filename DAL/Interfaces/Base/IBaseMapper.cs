﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Interfaces.Base
{
    public interface IBaseMapper
    {
        TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class;
    }

}
