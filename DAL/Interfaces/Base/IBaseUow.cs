﻿using Domain.Interfaces;
using System.Threading.Tasks;

namespace DAL.Interfaces.Base
{
    public interface IBaseUow
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();

        IBaseRepository<TDALEntity> BaseRepository<TDALEntity, TDomainEntity>()
            where TDomainEntity : class, IBaseEntity, new()
            where TDALEntity : class, new();
    }
}
