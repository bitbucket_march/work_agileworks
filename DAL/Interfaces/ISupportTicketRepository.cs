﻿using DAL.Interfaces.Base;
using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface ISupportTicketRepository : ISupportTicketRepository<SupportTicketDALDTO>
    {}
    public interface ISupportTicketRepository<TDALEntity> : IBaseRepository<TDALEntity>
        where TDALEntity : class, new()
    {
        Task<List<SupportTicketDALDTO>> GetAllVisibleTicketsAsync();
    }
}
