﻿using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;

namespace DAL.Interfaces.Helpers
{
    // Andres Käver @ ITCollege
    public interface IBaseRepositoryFactory<TDbContext>
        where TDbContext : DbContext
    {
        void AddToCreationMethods<TRepository>(Func<TDbContext, TRepository> creationMethod)
            where TRepository : class;

        Func<TDbContext, object> GetRepositoryFactory<TRepository>();

        Func<TDbContext, object> GetEntityRepositoryFactory<TDALEntity, TDomainEntity>()
            where TDALEntity : class, new()
            where TDomainEntity : class, IBaseEntity, new();
    }

}
