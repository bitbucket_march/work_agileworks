﻿using DAL.Interfaces.Base;
using Domain.Interfaces;

namespace DAL.Interfaces.Helpers
{
    // Andres Käver @ ITCollege
    public interface IBaseRepositoryProvider
    {
        TRepository GetRepository<TRepository>();

        IBaseRepository<TDALEntity> GetEntityRepository<TDALEntity, TDomainEntity>()
            where TDALEntity : class, new()
            where TDomainEntity : class, IBaseEntity, new();
    }

}
