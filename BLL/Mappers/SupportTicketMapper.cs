﻿using BLL.Interfaces.Base;
using DTO;
using System;


namespace BLL.Mappers
{
    public class SupportTicketMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject)
            where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(SupportTicketBLLDTO))
            {
                return MapFromDAL((SupportTicketDALDTO) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(SupportTicketDALDTO))
            {
                return MapFromBLL((SupportTicketBLLDTO) inObject) as TOutObject;
            }
            throw new InvalidCastException($"Can't convert from {inObject.GetType().FullName} to {typeof(TOutObject).FullName}");

        }

        public static SupportTicketBLLDTO MapFromDAL(SupportTicketDALDTO ticket)
        {
            var res = ticket == null ? null : new SupportTicketBLLDTO
            {
                Id = ticket.Id,
                Title = ticket.Title,
                Body = ticket.Body,
                TimeCreated = ticket.TimeCreated,
                DueTime = ticket.DueTime,
                IsCritical = ((ticket.DueTime - DateTime.Now).TotalHours < 1) || DateTime.Now > ticket.DueTime ? true : false
            };

            return res;
        }

        public static SupportTicketDALDTO MapFromBLL(SupportTicketBLLDTO ticket)
        {
            var res = ticket == null ? null : new SupportTicketDALDTO
            {
                Id = ticket.Id,
                Title = ticket.Title,
                Body = ticket.Body,
                TimeCreated = ticket.TimeCreated,
                DueTime = ticket.DueTime,
                IsVisible = !ticket.IsCompleted
            };
            return res;
        }

    }
}
