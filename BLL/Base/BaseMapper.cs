﻿using AutoMapper;
using BLL.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Base
{
    public class BaseMapper<TBLLEntity, TDALEntity> : IBaseBLLMapper
        where TBLLEntity : class, new()
        where TDALEntity : class, new()

    {
        private readonly IMapper _mapper;

        public BaseMapper()
        {
            _mapper = new MapperConfiguration(config =>
            {
                config.CreateMap<TBLLEntity, TDALEntity>();
                config.CreateMap<TDALEntity, TBLLEntity>();
            }
            ).CreateMapper();
        }

        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            return _mapper.Map<TOutObject>(inObject);
        }
    }

}
