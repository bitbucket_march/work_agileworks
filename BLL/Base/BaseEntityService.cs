﻿using BLL.Interfaces.Base;
using DAL.Interfaces;
using DAL.Interfaces.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Base
{
    public abstract class BaseEntityService<TBLLEntity,TDALEntity, TUow> : IBaseService,
        IBaseEntityService<TBLLEntity>
        where TBLLEntity : class, new()
        where TDALEntity : class, new()
        where TUow : IUow
    {
        protected readonly TUow _uow;
        protected IBaseRepository<TDALEntity> _repository;
        private readonly IBaseBLLMapper _mapper;

        public BaseEntityService(TUow uow, IBaseBLLMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        public virtual TBLLEntity Update(TBLLEntity entity)
        {
            return _mapper.Map<TBLLEntity>(_repository.Update(_mapper.Map<TDALEntity>(entity)));
        }

        public virtual void Remove(TBLLEntity entity)
        {
            _repository.Remove(_mapper.Map<TDALEntity>(entity));
        }

        public virtual void Remove(params object[] id)
        {
            _repository.Remove(id);
        }

        public virtual async Task<List<TBLLEntity>> AllAsync()
        {
            return (await _repository.AllAsync()).Select(e => _mapper.Map<TBLLEntity>(e)).ToList();
        }

        public virtual async Task<TBLLEntity> FindAsync(params object[] id)
        {
            return _mapper.Map<TBLLEntity>(await _repository.FindAsync(id));
        }

        public virtual async Task<TBLLEntity> AddAsync(TBLLEntity entity)
        {
            return _mapper.Map<TBLLEntity>(await _repository.AddAsync(_mapper.Map<TDALEntity>(entity)));
        }

        public List<TBLLEntity> All()
        {
            return _repository.All().Select(e => _mapper.Map<TBLLEntity>(e)).ToList();
        }

        public TBLLEntity Find(params object[] id)
        {
            return _mapper.Map<TBLLEntity>(_repository.Find(id));
        }

        public TBLLEntity Add(TBLLEntity entity)
        {
            return _mapper.Map<TBLLEntity>(_repository.Add(_mapper.Map<TDALEntity>(entity)));
        }

    }
}