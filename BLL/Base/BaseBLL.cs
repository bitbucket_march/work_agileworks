﻿using BLL.Interfaces.Base;
using DAL.Interfaces;
using DAL.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Base
{
    public class BaseBLL<TUow> : IBaseBLL
            where TUow : IUow
    {
        protected readonly TUow _uow;

        public BaseBLL(TUow uow)
        {
            _uow = uow;
        }
        public virtual async Task<int> SaveChangesAsync()
        {
            return await _uow.SaveChangesAsync();
        }

        public int SaveChanges()
        {
            return _uow.SaveChanges();
        }
    }

}
