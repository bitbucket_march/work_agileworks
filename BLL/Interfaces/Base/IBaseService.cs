﻿using DAL.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces.Base
{
    public interface IBaseService 
    {
    }
    public interface IBaseEntityService<TBLLEntity> : IBaseService, IBaseRepository<TBLLEntity>
        where TBLLEntity : class, new()
    {
    }


}
