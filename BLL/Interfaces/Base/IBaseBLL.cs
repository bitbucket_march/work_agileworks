﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces.Base
{
    public interface IBaseBLL
    {
        Task<int> SaveChangesAsync();
        int SaveChanges();
    }
}
