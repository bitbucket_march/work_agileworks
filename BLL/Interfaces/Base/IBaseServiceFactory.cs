﻿using DAL.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces.Base
{
    public interface IBaseServiceFactory<TUnitOfWork>
            where TUnitOfWork : IBaseUow
    {
        void AddToCreationMethods<TService>(Func<TUnitOfWork, TService> creationMethod)
            where TService : class;

        Func<TUnitOfWork, object> GetServiceFactory<TService>();

    }
}
