﻿using BLL.Interfaces.Base;
using BLL.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces
{
    public interface IBLL : IBaseBLL
    {
        ISupportTicketsService Tickets { get; }
    }
}
