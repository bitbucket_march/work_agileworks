﻿using BLL.Interfaces.Base;
using DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces.Services
{
    public interface ISupportTicketsService : IBaseEntityService<SupportTicketBLLDTO>
    {
        SupportTicketBLLDTO CloseTicket(SupportTicketBLLDTO supportTicket);
        Task<List<SupportTicketBLLDTO>> GetAllVisibleTicketsAsync();
    }
}
