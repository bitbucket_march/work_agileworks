﻿using BLL.Interfaces.Services;
using DAL.Interfaces;
using DTO;
using BLL.Mappers;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace BLL.Services
{
    public class SupportTicketsService : 
        Base.BaseEntityService<SupportTicketBLLDTO, SupportTicketDALDTO, IUow>, 
        ISupportTicketsService
    {
        public SupportTicketsService(IUow uow) : base(uow, new SupportTicketMapper())
        {
            _repository = uow.Tickets;
        }

        public override Task<SupportTicketBLLDTO> AddAsync(SupportTicketBLLDTO entity)
        {
            entity.TimeCreated = DateTime.Now;
            return base.AddAsync(entity);
        }
        public SupportTicketBLLDTO CloseTicket(SupportTicketBLLDTO entity)
        {
            entity.IsCompleted = true;
            return base.Update(entity);
        }
        public async Task<List<SupportTicketBLLDTO>> GetAllVisibleTicketsAsync()
        {
            return (await _uow.Tickets.GetAllVisibleTicketsAsync())
                .Select(s => SupportTicketMapper.MapFromDAL(s))
                .ToList();
        }

    }
}
