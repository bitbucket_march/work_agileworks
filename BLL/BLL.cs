﻿using BLL.Base;
using BLL.Interfaces;
using BLL.Interfaces.Services;
using BLL.Services;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
namespace BLL
{
    public class BLL : BaseBLL<IUow>, IBLL
    {
        protected readonly IUow _uow;

        public BLL(IUow uow) : base(uow)
        {
            _uow = uow;
        }

        public ISupportTicketsService Tickets => GetOrCreateRService<ISupportTicketsService>(_uow => new SupportTicketsService(_uow));

        private readonly Dictionary<Type, object> _serviceCache = new Dictionary<Type, object>();

        private TRepo GetOrCreateRService<TRepo>(Func<IUow, TRepo> serviceCreationMethod)
        {
            if (_serviceCache.ContainsKey(typeof(TRepo)))
            {
                return (TRepo)_serviceCache[typeof(TRepo)];
            }

            var repoObj = serviceCreationMethod(_uow);
            _serviceCache.Add(typeof(TRepo), repoObj);
            return repoObj;
        }
    }
}
