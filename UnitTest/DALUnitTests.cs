﻿using DTO;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest
{
    public class DALUnitTests
    {
        TestHelper testHelper;
        public DALUnitTests()
        {
            testHelper = new TestHelper();
        }

        // DAL Adds 5 tickets, gets 5 tickets
        [Fact]
        public async Task uowAddSaves5Tickets()
        {
            var testList = getTestDALTicketList();

            // Add 5 tickets to DB
            var testUOW = testHelper.GetNewTestUOW();
            foreach (var ticket in testList)
            {
                await testUOW.Tickets.AddAsync(ticket);
            }
            await testUOW.SaveChangesAsync();

            // In a new context try to get all tickets from DB
            testUOW = testHelper.GetNewTestUOW();
            List<SupportTicketDALDTO> supportTickets = await testUOW.Tickets.AllAsync();

            supportTickets.Count.ShouldBe(5);
        }

        // DAL Adds 5 tickets, returns 4 visible tickets ordered by deadline
        [Fact]
        public async Task uowGetsVisibleTicketsOrderedByDeadline()
        {
            var testList = getTestDALTicketList();
            testList.Reverse();
            testList[0].IsVisible = false;
            /*
            * testList items:
            * Due in a year - not visible
            * Due in 2 hrs
            * Due in 3 hrs
            * 1 min over deadline
            * Due in hour
            */

            //For later comparison
            var correctList = testList;
            correctList.RemoveAt(0);
            correctList = correctList.OrderBy(o => o.DueTime).ToList();
            /* 
            * correctList items:
            * 1 min over deadline
            * Due in hour
            * Due in 2 hrs
            * Due in 3 hrs
            */

            // Add 5 tickets to DB
            var testUOW = testHelper.GetNewTestUOW();
            foreach (var ticket in testList)
            {
                await testUOW.Tickets.AddAsync(ticket);
            }
            await testUOW.SaveChangesAsync();

            // In a new context try to get all visible and sorted tickets from DB
            testUOW = testHelper.GetNewTestUOW();
            List<SupportTicketDALDTO> supportTickets = await testUOW.Tickets.GetAllVisibleTicketsAsync();
            for (int i = 0; i < supportTickets.Count; i++)
            {
                supportTickets[i].Title.ShouldBe(correctList[i].Title);
            }
        }

        List<SupportTicketDALDTO> getTestDALTicketList()
        {
            var entityList = new List<SupportTicketDALDTO>();
            entityList.Add(new SupportTicketDALDTO
            {
                Title = "Due in hour",
                Body = "Should be red in UI",
                DueTime = DateTime.Now.AddMinutes(59),
                TimeCreated = DateTime.Now,
                IsVisible = true
            });
            entityList.Add(new SupportTicketDALDTO
            {
                Title = "1 min over deadline",
                Body = "Should be red in UI",
                DueTime = DateTime.Now.AddMinutes(-1),
                TimeCreated = DateTime.Now,
                IsVisible = true
            });
            entityList.Add(new SupportTicketDALDTO
            {
                Title = "Due in 3 hrs",
                Body = "Should be 4th in table",
                DueTime = DateTime.Now.AddHours(3),
                TimeCreated = DateTime.Now,
                IsVisible = true
            });
            entityList.Add(new SupportTicketDALDTO
            {
                Title = "Due in 2 hrs",
                Body = "Should be 3rd in table",
                DueTime = DateTime.Now.AddHours(2),
                TimeCreated = DateTime.Now,
                IsVisible = true
            });
            entityList.Add(new SupportTicketDALDTO
            {
                Title = "Due in a year",
                Body = "Should be last in table",
                DueTime = DateTime.Now.AddYears(1),
                TimeCreated = DateTime.Now,
                IsVisible = true
            });

            return entityList;
        }
    }
}
