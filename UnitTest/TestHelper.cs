﻿using BLL.Interfaces;
using CSupportWebApp.Controllers;
using DAL;
using DAL.Helpers;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace UnitTest
{
    public class TestHelper
    {
        IUow testUOW;
        IBLL testBLL;

        AppDbContext testDBContext;
        DbContextOptions options;

        public TestHelper()
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .UseInternalServiceProvider(serviceProvider)
                .Options;
        }

        private void SetNewDBContext()
        {
            testDBContext = new AppDbContext(options);
        }

        private void SetNewUOW()
        {
            SetNewDBContext();
            testUOW = new Uow(testDBContext, new BaseRepositoryProvider<AppDbContext>(new AppRepositoryFactory(), testDBContext));
        }

        private void SetNewBLL()
        { 
            SetNewUOW();
            testBLL = new BLL.BLL(testUOW);
        }

        public IUow GetNewTestUOW()
        {
            SetNewUOW();
            return testUOW;
        }

        public IBLL GetNewTestBLL()
        {
            SetNewBLL();
            return testBLL;
        }

        public TicketsController GetNewTestTicketsController()
        {
            SetNewBLL();
            return new TicketsController(testBLL);
        }
    }
}
