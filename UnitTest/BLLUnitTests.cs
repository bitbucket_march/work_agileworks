using DTO;
using Xunit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shouldly;

namespace UnitTest
{
    public class BLLUnitTests
    {
        TestHelper testHelper;

        public BLLUnitTests()
        {
            testHelper = new TestHelper();
        }

        // BLL Adds 5 tickets, gets 5 tickets with current creation time
        [Fact]
        public async Task bllAddsTicketsWithCurrentTime()
        {
            var testList = getTestBLLTicketList();

            // Add 5 tickets to DB
            var testBLL = testHelper.GetNewTestBLL();
            foreach (var ticket in testList)
            {
                await testBLL.Tickets.AddAsync(ticket);
            }
            await testBLL.SaveChangesAsync();

            // Get tickets from 
            testBLL = testHelper.GetNewTestBLL();
            var ticketList = await testBLL.Tickets.GetAllVisibleTicketsAsync();
            foreach (var ticket in ticketList)
            {
                ticket.TimeCreated.Second.ShouldBeLessThan(60);
            }
        }

        // BLL Adds 5 tickets, closes random one, gets 4 tickets excluding the random.
        [Fact]
        public async Task bllClosesTicket()
        {
            var testList = getTestBLLTicketList();

            // Add 5 tickets to DB
            var testBLL = testHelper.GetNewTestBLL();

            foreach (var ticket in testList)
            {
                await testBLL.Tickets.AddAsync(ticket);
            }
            await testBLL.SaveChangesAsync();

            // Close random ticket
            int randomId = new Random().Next(1, 6);
            testBLL = testHelper.GetNewTestBLL();

            var ticketToClose = await testBLL.Tickets.FindAsync(randomId);
            testBLL.Tickets.CloseTicket(ticketToClose);

            await testBLL.SaveChangesAsync();

            // Get tickets from DB, ensure random closed ticket not in resultset
            testBLL = testHelper.GetNewTestBLL();

            var ticketList = await testBLL.Tickets.GetAllVisibleTicketsAsync();
            ticketList.Any(o => o.Id == randomId).ShouldBeFalse();
            ticketList.Count.ShouldBe(4);

        }

        // BLL Adds 5 tickets, upon mapping from DAL, tickets due in hour or less get IsCritical status
        [Fact]
        public async Task bllInLessThanHourDueTicketsAreCritical()
        {
            var testList = getTestBLLTicketList();

            // Add 5 tickets to DB
            var testBLL = testHelper.GetNewTestBLL();
            foreach (var ticket in testList)
            {
                await testBLL.Tickets.AddAsync(ticket);
            }
            await testBLL.SaveChangesAsync();

            // Get tickets from DB, ensure tickets with less than hour til deadline are critical
            testBLL = testHelper.GetNewTestBLL();

            var ticketList = await testBLL.Tickets.GetAllVisibleTicketsAsync();
            var criticalList = ticketList.Where(o => (o.DueTime - DateTime.Now).TotalHours < 1 || DateTime.Now > o.DueTime)
                .ToList();
            criticalList.All(o => o.IsCritical).ShouldBeTrue();
            ticketList.Where(o => (o.DueTime - DateTime.Now).TotalMinutes > 60)
                .ToList().Count.ShouldBe(3);
        }

        List<SupportTicketBLLDTO> getTestBLLTicketList()
        {
            var entityList = new List<SupportTicketBLLDTO>();
            entityList.Add(new SupportTicketBLLDTO
            {
                Title = "Due in hour",
                Body = "Should be red in UI",
                DueTime = DateTime.Now.AddMinutes(59),
            });
            entityList.Add(new SupportTicketBLLDTO
            {
                Title = "1 min over deadline",
                Body = "Should be red in UI",
                DueTime = DateTime.Now.AddMinutes(-1),
            });
            entityList.Add(new SupportTicketBLLDTO
            {
                Title = "Due in 3 hrs",
                Body = "Should be 4th in table",
                DueTime = DateTime.Now.AddHours(3),
            });
            entityList.Add(new SupportTicketBLLDTO
            {
                Title = "Due in 2 hrs",
                Body = "Should be 3rd in table",
                DueTime = DateTime.Now.AddHours(2),
            });
            entityList.Add(new SupportTicketBLLDTO
            {
                Title = "Due in a year",
                Body = "Should be last in table",
                DueTime = DateTime.Now.AddYears(1),
            });

            return entityList;
        }
    }
}
